const Joi = require('joi');

const authSchema = Joi.object({
    user_name: Joi.string().required(),
    email: Joi.string().email().lowercase().required(),
    password: Joi.string()
        .pattern(new RegExp("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$"))
});

const loginAuthSchema = Joi.object({
    email: Joi.string().email().lowercase().required(),
    password: Joi.string()
});

module.exports = {
    authSchema,
    loginAuthSchema
};
