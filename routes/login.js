const express = require("express");
const router = express.Router();
const Users = require('../models/Users');
const bcryptjs = require("bcryptjs");
const { loginAuthSchema } = require("../helpers/validate");
const jwt = require('jsonwebtoken');
const env = process.env.NODE_ENV || 'development';
const config = require("../config/config.js")[env];

router.post('/', async (req, res) => {
    try {
        const result = await loginAuthSchema.validateAsync(req.body);
        const email = req.body.email;
        const enteredPassword = req.body.password;
        const { password } = await Users.findOne({
            attributes: ['password'], where: {
                email: email
            }
        });
        
        const isPasswordCorrect = await bcryptjs.compare(enteredPassword, password);

        if (isPasswordCorrect) {
            const secretkey = config.TOKEN_SECRET;
            const refreshSecretKey = config.REFRESH_TOKEN_SECRET;
            const accessToken = jwt.sign({ email }, secretkey, { expiresIn: '2d' });
            const refreshToken = jwt.sign({ email }, refreshSecretKey);

            return res.send({
                accessToken,
                refreshToken
            });

        } else {
            throw new Error("Password Incorrect");
        }
    } catch (err) {
        if (err.message === "Password Incorrect") {
            return res.status(400).send({
                msg: err.message
            });
        }
        return res.status(400).send({
            msg: "User doesn't exist"
        });
    }
});


module.exports = router;