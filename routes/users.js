const express = require('express');
const router = express.Router();
const Users = require('../models/Users');


router.get("/", (req, res) =>
    Users.findAll()
        .then(users => {
            res.sendStatus(200);
        })
        .catch(err => console.log(err)));

module.exports = router;