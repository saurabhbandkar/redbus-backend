const express = require("express");
const router = express.Router();
const Users = require('../models/Users');
const TempUser = require('../models/TempUser');
const env = process.env.NODE_ENV || 'development';
const config = require("../config/config.js")[env];
const jwt = require("jsonwebtoken")

router.post("/", async (req, res) => {
    try {
        let { OTP, email } = req.body;
        console.log(email);
        isPasswordCorrect = true
        TempUser.findAll({
            attributes: ['user_name', "email", "password", "OTP"],
            where: {
                email: email
            },
            raw: true
        }).then(async (result) => {
            const OTParray = [];
            const mailArray = [];
            const userNameArray = [];
            const passwordArray = [];
            if (result) {
                result.map((user) => {
                    OTParray.push(user.OTP)
                    mailArray.push(user.email)
                    passwordArray.push(user.password)
                    userNameArray.push(user.user_name)
                })
                console.log(OTParray)
                // if (OTP != OTParray[OTParray.length - 1]) {

                // return res.status(401).send({
                //     msg: " OTP Incorrect, please try again"
                // })
                // } else {
                const secretkey = config.TOKEN_SECRET;
                const refreshSecretKey = config.REFRESH_TOKEN_SECRET;
                const email = mailArray[mailArray.length - 1];
                const user_name = userNameArray[userNameArray.length - 1];
                const password = passwordArray[passwordArray.length - 1];
                const accessToken = jwt.sign({ email }, secretkey, { expiresIn: '2d' });
                const refreshToken = jwt.sign({ email }, refreshSecretKey);
                const user = await Users.create({ user_name, email, password });
                return res.status(200)
                    .send({
                        accessToken,
                        refreshToken
                    });
            } else {
                res.status(404).send({ msg: "Data not found" });
            }
        }).catch((error) => {
            console.log(error);
        })

    } catch (err) {
        if (err.message === "OTP Incorrect, please try again") {
            return res.status(400).send({
                msg: err.message
            });
        }
        return res.status(500).send({
            msg: err.message
        });
    };
})

module.exports = router;