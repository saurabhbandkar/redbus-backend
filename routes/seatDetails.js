const express = require("express");
const router = express.Router();
const Seats = require('../models/Seats');
const Bus = require('../models/Bus');
const env = process.env.NODE_ENV || 'development';
const authenticateJWT = require("../auth");

router.post('/', async (req, res) => {
    try {
        let { busId } = req.body;

        if (!busId) {
            return res.status(400).send({
                msg: "Please select correct bus"
            });
        } else {
            const availableSeats = await Seats.findAll({
                where: {
                    bus_id: busId
                },
                raw: true
            });

            if (availableSeats.length != 0) {
                return res.status(200).send({
                    availableSeats
                });
            } else {
                return res.status(400).send({
                    msg: "No seats available for this bus"
                });
            };
        }
    }
    catch (err) {
        return res.status(400).send({
            msg: err.message
        });
    };
});

module.exports = router;