const express = require("express");
const router = express.Router();
const jwt = require('jsonwebtoken');
const env = process.env.NODE_ENV || 'development';
const config = require("../config/config.js")[env];


router.post('/refreshtoken', (req, res) => {
    const { refreshToken } = req.body;
    const secretkey = config.TOKEN_SECRET;
    const refreshSecretKey = config.REFRESH_TOKEN_SECRET;

    if (!refreshToken) {
        return res.sendStatus(401);
    }

    jwt.verify(refreshToken, refreshSecretKey, (err, user) => {
        if (err) {
            return res.sendStatus(403);
        }

        const accessToken = jwt.sign(user, secretkey, { expiresIn: '2d' });

        res.json({
            accessToken
        });
    })

});

module.exports = router;

