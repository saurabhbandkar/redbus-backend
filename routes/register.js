const express = require("express");
const router = express.Router();
const TempUser = require('../models/TempUser');
const Users = require('../models/Users');
const bcryptjs = require("bcryptjs");
const { authSchema } = require("../helpers/validate");
const env = process.env.NODE_ENV || 'development';
const config = require("../config/config.js")[env];
const sgMail = require('@sendgrid/mail')



router.post("/", async (req, res) => {

    function createOTP() {
        let minm = 100000;
        let maxm = 999999;
        let OTP = Math.floor(Math.random() * (maxm - minm + 1)) + minm;
        return OTP;
    }
    function sendMail(email, user_name, password) {

        const OTP = createOTP();
        sgMail.setApiKey(config.API_KEY)
        const msg = {
            to: email,
            from: 'saurabh.bandkar.16@mountblue.tech',
            subject: 'Login verification for redbus.com',
            text: 'Successfully integrated sendgrid mail.',
            html: `OTP for verifying your email address is <strong>${OTP}</strong>`,
        }
        sgMail
            .send(msg)
            .then(() => {
                const user = TempUser.create({ user_name, email, password, OTP });
                res.status(200).send({
                    msg: "Email sent successfully"
                });
                const user1 = Users.create({ user_name, email, password });
            })
            .catch((error) => {
                console.error(error)
            })

    };


    try {
        const saltRounds = 10;
        let { user_name, email, password } = req.body;
        // await Users.findAll({
        //     attributes: ["email"],
        //     where: {
        //         email: email
        //     }
        // })
        //     .then((mailResult) => {
        //         if (mailResult.length > 0) {
        //             throw new Error("Email Id is already registered with redbus.com")
        //         };
        //     })

        const result = await authSchema.validateAsync(req.body);
        password = await bcryptjs.hash(password, saltRounds);
        sendMail(email, user_name, password);


    } catch (err) {
        if (err.message === "\"email\" must be a valid email") {
            return res.status(400).send({
                msg: "Entered email does not exist"
            });
        } else if (err.message === "Validation error") {
            return res.status(400).send({
                msg: "Email Id is already registered with redbus.com"
            });
        }
        return res.status(500).send({
            msg: err.message
        });
    };
})

module.exports = router;