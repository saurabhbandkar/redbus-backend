const express = require("express");
const router = express.Router();
const Passengers = require('../models/Passengers');
const BookingDetails = require('../models/BookingDetails');
const Seats = require("../models/Seats");
const Bus = require("../models/Bus");
const Users = require("../models/Users");
const { v4: uuidv4 } = require('uuid');
const sgMail = require('@sendgrid/mail')
const env = process.env.NODE_ENV || 'development';
const config = require("../config/config.js")[env];
const jwt = require("jsonwebtoken")
const authenticateJWT = require("../auth.js");

function sendMail(mailAddress, [
    boardingPoint, droppingPoint, passengers, travelDate, ticketNumber
]) {

    sgMail.setApiKey(config.API_KEY)
    const msg = {
        to: mailAddress,
        from: 'saurabh.bandkar.16@mountblue.tech',
        subject: 'Ticket Details | redbus.com',
        text: 'Successful acknowlegement.',
        html: `<strong> Congratulations !! </strong>`
            + `" Ticket number is: ${ticketNumber}`
            + `" Boarding Point is: ${boardingPoint}`
            + `" Dropping Point is: ${droppingPoint}`
            + `" Date of Travel is: ${travelDate}`
            + `" Total passengers: ${passengers}`
    }
    sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent')
        })
        .catch((error) => {
            console.error(error)
        })
};


router.post('/', authenticateJWT, async (req, res) => {
    try {
        const ticketNumber = uuidv4();
        let bookingId = 1;
        let {
            busId,
            boardingPoint,
            droppingPoint,
            totalFare,
            passengers
        } = req.body;
        let totalPassengers = passengers.length;

        await Bus.findOne(
            {
                attributes: ["available_seat", "travel_date"], where: {
                    id: busId
                },
                raw: true
            }).then(async (seatsData) => {

                const authHeader = req.headers.authorization;
                if (authHeader) {
                    const secretkey = config.TOKEN_SECRET;
                    const token = authHeader.split(' ')[1];
                    const decoded = jwt.verify(token, secretkey);
                    const mailAddress = decoded.email;

                    sendMail(mailAddress, [boardingPoint, droppingPoint, totalPassengers, seatsData.travel_date, ticketNumber]);
                };

                await Bus.update(
                    {
                        available_seat: seatsData.available_seat - totalPassengers
                    },
                    {
                        where: {
                            id: busId
                        }
                    }).then(() => {
                    }).catch((error) => {
                        console.error(error);
                    })
            }).catch((error) => {
                console.error(error);
            });


        await passengers.map(async (passenger) => {
            await Seats.findOne(
                {
                    attributes: ['id'], where: {
                        bus_id: busId,
                        seatname: passenger.seatName
                    },
                    raw: true
                }).then((seatsData) => {
                    Passengers.create(
                        {
                            seat_id: parseInt(seatsData.id),
                            booking_id: bookingId,
                            age: passenger.age,
                            passenger_name: passenger.name,
                            gender: passenger.gender
                        }).then((passengersData) => {
                        }).catch((error) => {
                            console.error(error);
                        })

                    Seats.update(
                        {
                            is_booked: 1
                        },
                        {
                            where: {
                                id: seatsData.id
                            }
                        }).then((seatsData) => {
                        }).catch((error) => {
                            console.error(error);
                        })
                }).catch((error) => {
                    console.log(error)
                })
        });
        return res.status(200).send({
            msg: "Ticket is booked and sent to user successfully!!"
        });
    } catch (err) {
        return res.status(400).send({
            msg: err.message
        });
    }
});


module.exports = router;
