const express = require("express");
const router = express.Router();
const db = require("../config/database");
const Cities = require('../models/Cities');
const Bus = require('../models/Bus');
const Stops = require("../models/Stops");
const BookingDetails = require("../models/BookingDetails");
const Passengers = require("../models/Passengers");
const Users = require("../models/Users");
const env = process.env.NODE_ENV || 'development';

function timeDiffCalc(arrival, departure) {
    let diffInMilliSeconds = Math.abs(arrival - departure) / 1000;
    const days = Math.floor(diffInMilliSeconds / 86400);
    diffInMilliSeconds -= days * 86400;
    const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
    diffInMilliSeconds -= hours * 3600;
    const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
    diffInMilliSeconds -= minutes * 60;

    let difference = '';
    if (days > 0) {
        difference += (days === 1) ? `${days} DD, ` : `${days} DD, `;
    }
    difference += (hours === 0 || hours === 1) ? `${hours} HH, ` : `${hours} HH, `;

    difference += (minutes === 0 || hours === 1) ? `${minutes} MM` : `${minutes} MM`;

    return difference;
}

router.post('/', async (req, res) => {
    try {

        let { from, to, date } = req.body;

        const sourceCity = await Cities.findOne({
            attributes: ['id'], where: {
                city_name: from
            }
        });
        const destinationCity = await Cities.findOne({
            attributes: ['id'], where: {
                city_name: to
            }
        });

        if (!sourceCity || !destinationCity) {
            return res.status(400).send({
                msg: "Please select correct city"
            });
        } else {
            const stopDetails = await Bus.findAll({
                attributes: ['id'],
                where: {
                    source_id: sourceCity.id,
                    destination_id: destinationCity.id,
                    travel_date: date
                },
                raw: true,
                include: [{
                    attributes: ['boarding_point', 'dropping_point'],
                    model: Stops,
                    required: true
                }]
            });

            const boardingPoint = [];
            const droppingPoint = [];
            stopDetails.map((stop) => {
                boardingPoint.push([stop["id"], stop["stops.boarding_point"].split(",")]);
                droppingPoint.push([stop["id"], stop["stops.dropping_point"].split(",")]);
            });
            console.log(boardingPoint, droppingPoint)
            const availableBuses = await Bus.findAll({
                where: {
                    source_id: sourceCity.id,
                    destination_id: destinationCity.id,
                    travel_date: date
                },
                raw: true
            });

            if (availableBuses.length != 0) {
                availableBuses.map((bus) => {
                    delete bus.source_id;
                    delete bus.destination_id;
                    bus.source_city = from;
                    bus.destination_city = to;
                    bus.duration = timeDiffCalc(new Date(bus.arrival), new Date(bus.departure));
                    bus.travel_date = bus.travel_date.toString().slice(0, 15);
                    bus.departure_date = bus.departure.toString().slice(4, 15);
                    bus.arrival_date = bus.arrival.toString().slice(4, 15);
                    bus.departure = bus.departure.toString().slice(16, 21);
                    bus.arrival = bus.arrival.toString().slice(16, 21);

                    boardingPoint.map((arrayElement) => {
                        if (bus.id == parseInt(arrayElement[0])) {
                            bus.boarding_point = arrayElement[1];
                        }
                    });
                    droppingPoint.map((arrayElement) => {
                        if (bus.id == arrayElement[0]) {
                            bus.dropping_point = arrayElement[1];
                        }
                    });

                });
                return res.status(200).send({
                    availableBuses
                });
            }
        }
    } catch (err) {
        return res.status(400).send({
            msg: err.message
        });
    }
});


module.exports = router;
