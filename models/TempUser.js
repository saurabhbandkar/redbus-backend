const { DataTypes } = require('sequelize');
const db = require("../config/database");

const TempUser = db.define("tempUser", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    user_name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'User must have a name' },
            notEmpty: { msg: 'User name cannot be empty' }
        }
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true,
            notNull: { msg: 'User must have an email' },
            notEmpty: { msg: 'Email cannot be empty' },
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'User must have a password' },
            notEmpty: { msg: 'Password cannot be empty' }
        }
    },
    OTP: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
            notNull: { msg: 'User must have an OTP' },
            notEmpty: { msg: 'OTP cannot be empty' }
        }
    }
});


module.exports = TempUser;