const { DataTypes } = require('sequelize');
const db = require("../config/database");

const Cities = db.define("cities", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    city_name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'City must have a name' },
            notEmpty: { msg: 'City name cannot be empty' }
        }
    },
    country: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Country must have a name' },
            notEmpty: { msg: 'Country name cannot be empty' }
        }
    },
    state: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'State must have a name' },
            notEmpty: { msg: 'State name cannot be empty' }
        }
    }
});

 
module.exports = Cities;