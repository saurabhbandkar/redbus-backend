const { Sequelize, DataTypes } = require('sequelize');
const db = require("../config/database");
const Amenities = require("./Amenities");

const BusAmenities = db.define("bus_amenities", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    bus_id_amenitiy: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: { msg: 'Bus must have an id' },
            notEmpty: { msg: 'Bus id cannot be empty' }
        }
    },
    amenity_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            notNull: { msg: 'Bus must have an amenity' },
            notEmpty: { msg: 'Amenity Id cannot be empty' }
        }
    }
});

BusAmenities.hasOne(Amenities, {
    foreignKey: {
        name: "amenity_id"
    }
});
Amenities.belongsTo(BusAmenities, {
    foreignKey: {
        name: "amenity_id"
    }
});



module.exports = BusAmenities;