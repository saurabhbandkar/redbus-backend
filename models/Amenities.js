const { DataTypes } = require('sequelize');
const db = require("../config/database");

const Amenities = db.define("amenities", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    amenity_name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Amenities must have a name' },
            notEmpty: { msg: 'Amenities name cannot be empty' }
        }
    }
});



module.exports = Amenities;