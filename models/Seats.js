const { DataTypes } = require('sequelize');
const db = require("../config/database");
const Passengers = require("./Passengers");

const Seats = db.define("seats", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    bus_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: { msg: 'Bus Id must be provided' },
            notEmpty: { msg: 'Bus Id cannot be empty' }
        }
    },
    seatname: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Seat must have a name' },
            notEmpty: { msg: 'Seat name cannot be empty' }
        }
    },
    is_booked: {
        type: DataTypes.TINYINT,
        allowNull: false,
        validate: {
            notNull: { msg: 'Booking status must be provided' },
            notEmpty: { msg: 'Booking Status cannot be empty' }
        }
    },
    gender: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Gender must be provided' },
            notEmpty: { msg: 'Gender cannot be empty' }
        }
    },
    is_single_deck: {
        type: DataTypes.TINYINT,
        allowNull: false,
        validate: {
            notNull: { msg: 'Deck must have a descritpion' },
            notEmpty: { msg: 'Deck description cannot be empty' }
        }
    }
});

Seats.hasOne(Passengers, {
    foreignKey: {
        name: "seat_id"
    }
});

Passengers.belongsTo(Seats, {
    foreignKey: {
        name: "seat_id"
    }
});
module.exports = Seats;