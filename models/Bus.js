const { DataTypes } = require('sequelize');
const db = require("../config/database");
const Stops = require("./Stops");
const Cities = require("./Cities");
const Seats = require("./Seats");
const BusAmenities = require("./BusAmenities");
const BookingDetails = require("./BookingDetails");

const Bus = db.define("bus", {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  bus_name: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: 'Bus must have a name' },
      notEmpty: { msg: 'Bus name cannot be empty' }
    }
  },
  source_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    unique: false,
    validate: {
      isEmail: false,
      notNull: { msg: 'Bus must have a source id' },
      notEmpty: { msg: 'SOurce id cannot be empty' }
    }
  },
  destination_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    unique: false,
    validate: {
      notNull: { msg: 'Bus must have a destination id' },
      notEmpty: { msg: 'Destination id cannot be empty' }
    }
  },
  travel_date: {
    type: DataTypes.DATEONLY,
    allowNull: false,
    unique: false,
    validate: {
      notNull: { msg: 'Bus must have a travel date' },
      notEmpty: { msg: 'Travel date cannot be empty' }
    }
  },
  departure: {
    type: DataTypes.DATE,
    allowNull: false,
    unique: false,
    validate: {
      notNull: { msg: 'Bus must have a departure time' },
      notEmpty: { msg: 'Departure date cannot be empty' }
    }
  },
  arrival: {
    type: DataTypes.DATE,
    allowNull: false,
    unique: false,
    validate: {
      notNull: { msg: 'Bus must have a arrival time' },
      notEmpty: { msg: 'Arrival date cannot be empty' }
    }
  },
  fare: {
    type: DataTypes.FLOAT,
    allowNull: false,
    validate: {
      notNull: { msg: 'Bus must have a fare' },
      notEmpty: { msg: 'Fare cannot be empty' }
    }
  },
  available_seat: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notNull: { msg: 'Bus must have available seats' },
      notEmpty: { msg: 'Available seats cannot be empty' }
    }
  },
  total_seat: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notNull: { msg: 'Bus must have total seats' },
      notEmpty: { msg: 'Total seats cannot be empty' }
    }
  },
  rating: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notNull: { msg: 'Bus must have a rating' },
      notEmpty: { msg: 'Rating cannot be empty' }
    }
  },
  is_single_deck: {
    type: DataTypes.TINYINT,
    allowNull: false,
    validate: {
      notNull: { msg: 'Bus must have a deck description' },
      notEmpty: { msg: 'Deck description cannot be empty' }
    }
  }
});

Bus.hasMany(Stops, {
  foreignKey: {
    name: "bus_id"
  }
});


Stops.belongsTo(Bus, {
  foreignKey: {
    name: "bus_id"
  }
});


Bus.hasMany(BookingDetails, {
  foreignKey: {
    name: "bus_id_booking"
  }
});

BookingDetails.belongsTo(Bus, {
  foreignKey: {
    name: "bus_id_booking"
  }
});


Bus.hasMany(Seats, {
  foreignKey: {
    name: "bus_id"
  }
});

Seats.belongsTo(Bus, {
  foreignKey: {
    name: "bus_id"
  }
});


Bus.hasOne(Cities, {
  foreignKey: {
    name: "source_id"
  }
});


Bus.hasOne(Cities, {
  foreignKey: {
    name: "destination_id"
  }
});

Cities.belongsTo(Bus, {
  foreignKey: {
    name: "source_id"
  }
});

Cities.belongsTo(Bus, {
  foreignKey: {
    name: "destination_id"
  }
});


Bus.hasMany(BusAmenities, {
  foreignKey: {
    name: "bus_id_amenity"
  }
});



BusAmenities.belongsTo(Bus, {
  foreignKey: {
    name: "bus_id_amenity"
  }
});

module.exports = Bus;