const { DataTypes } = require('sequelize');
const db = require("../config/database");
const BookingDetails = require("./BookingDetails");


const Passengers = db.define("passengers", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    seat_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: { msg: 'Seat Id must be provided' },
            notEmpty: { msg: 'Seat Id cannot be empty' }
        }
    },
    booking_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: { msg: 'Booking Id must be provided' },
            notEmpty: { msg: 'Booking Id cannot be empty' }
        }
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: { msg: 'Passenger age must be provided' },
            notEmpty: { msg: 'Passenger age cannot be empty' }
        }
    },
    passenger_name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Passenger name must be provided' },
            notEmpty: { msg: 'Passenger name cannot be empty' }
        }
    },
    gender: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Passenger must have a gender' },
            notEmpty: { msg: 'Gender cannot be empty' }
        }
    }
});

Passengers.hasOne(BookingDetails, {
    foreignKey: {
        name: "booking_id"
    }
});

BookingDetails.belongsTo(Passengers, {
    foreignKey: {
        name: "booking_id"
    }
});


module.exports = Passengers;