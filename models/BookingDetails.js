const { DataTypes } = require('sequelize');
const db = require("../config/database");
const Users = require("./Users");

const BookingDetails = db.define("booking_details", {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notNull: { msg: 'User must have an ID' },
      notEmpty: { msg: 'User ID cannot be empty' }
    }
  },
  bus_id_booking: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notNull: { msg: 'booking must have a bus id' },
      notEmpty: { msg: 'Bus id cannot be empty' }
    }
  },
  payment_status: {
    type: DataTypes.TINYINT,
    allowNull: false,
    validate: {
      notNull: { msg: 'Payment status must be provided' },
      notEmpty: { msg: 'Payment status cannot be empty' }
    }
  },
  ticket_number: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: 'Booking must have a ticket number' },
      notEmpty: { msg: 'Ticket number cannot be empty' }
    }
  },
  total_fare: {
    type: DataTypes.FLOAT,
    allowNull: false,
    validate: {
      notNull: { msg: 'Booking must have a fare amount' },
      notEmpty: { msg: 'Fare amount cannot be empty' }
    }
  },
  total_passengers: {
    type: DataTypes.INTEGER,
    allowNull: false,
    unique: false,
    validate: {
      notNull: { msg: 'Booking must have passenger details' },
      notEmpty: { msg: 'Passenger count cannot be empty' }
    }
  }
});

BookingDetails.hasOne(Users, {
  foreignKey: {
    name: "user_id"
  }
});

Users.belongsTo(BookingDetails, {
  foreignKey: {
    name: "user_id"
  }
});



module.exports = BookingDetails;