const { DataTypes } = require('sequelize');
const db = require("../config/database");


const Stops = db.define("stops", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    bus_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: { msg: 'Bus must have an id' },
            notEmpty: { msg: 'Bus id cannot be empty' }
        }
    },
    boarding_point: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Boarding point must have a name' },
            notEmpty: { msg: 'Boarding point cannot be empty' }
        }
    },
    dropping_point: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: { msg: 'Dropping point must have a name' },
            notEmpty: { msg: 'Dropping point cannot be empty' }
        }
    }
});



module.exports = Stops;