const dotenv = require('dotenv');
dotenv.config();
const express = require("express");
const db = require("./config/database");
const cors = require("cors");


const app = express();
app.use(cors({
    origin: "*"
}));

db.authenticate()
    .then(() => console.log("Database Connected"))
    .catch(err => console.log("Error: " + err));

app.use(express.json());


app.use('/users', require("./routes/users"));
app.use('/login', require("./routes/login"));
app.use('/register', require("./routes/register"));
app.use('/verifyotp', require("./routes/verifyOTP"));
app.use('/busdetails', require("./routes/busDetails"));
app.use('/seatdetails', require("./routes/seatDetails"));
app.use('/booktickets', require("./routes/bookTickets"));

const PORT = process.env.PORT || 9000;

app.listen(PORT, console.log(`Server started on port ${PORT}`));