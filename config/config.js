const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  "development": {
    "username": process.env.DBUSERNAME,
    "password": process.env.PASSWORD,
    "database": process.env.DATABASE,
    "host": process.env.HOST,
    "dialect": "mysql",
    "TOKEN_SECRET": process.env.TOKEN_SECRET,
    "REFRESH_TOKEN_SECRET": process.env.REFRESH_TOKEN_SECRET,
    "API_KEY": process.env.API_KEY
  },
  "production": {
    "username": process.env.DBUSERNAME,
    "password": process.env.PASSWORD,
    "database": process.env.DATABASE,
    "host": process.env.HOST,
    "dialect": "mysql",
    "TOKEN_SECRET": process.env.TOKEN_SECRET,
    "REFRESH_TOKEN_SECRET": process.env.REFRESH_TOKEN_SECRET,
    "API_KEY": process.env.API_KEY
  }
};
